package com.mpsc.humidity_sensor_producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HumiditySensorProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HumiditySensorProducerApplication.class, args);
	}

}
