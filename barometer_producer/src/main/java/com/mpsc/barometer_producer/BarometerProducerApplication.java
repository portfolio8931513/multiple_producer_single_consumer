package com.mpsc.barometer_producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BarometerProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BarometerProducerApplication.class, args);
	}

}
