# Multiple producer single consumer
> <span style="color:orange">&#9888;</span>**Disclaimer**: This application is intended to be a portfolio and learning project. It's not intended to be used as a functional product or part of one, thus the functionality is limited and the stability is not guaranteed.


## Introduction
This is a simple multiple producer - single consumer application for both portfolio and learning purposes. This application consist of:
* 3 dumb producer services that simulates data produced by 3 different sensors
* 1 consumer that deals with messages and stores them into a mongoDB database using reactive approach.

This is mainly an IoT-backend portfolio demo. 

The features to showcase and study with this project are:
* To explore and use the real time communications properties of RabbitMQ with dealing with queues and exchanges, persistence and error handling.
* Reactive programming to deal with mostly I/O operations to improve scalability
* Explore some features new features Java 21+ regarding ["data oriented programming"](https://inside.java/2024/02/12/voxxeddayscern-data-oriented-programming/) which main focus relays on modeling and managing data in an efficient, secure and reliable way by applying concepts such as data immutability and type exhaustiveness checking to ensure illegal states and unknown behaviour are not possible.



