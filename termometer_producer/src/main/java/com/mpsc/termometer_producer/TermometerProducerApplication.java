package com.mpsc.termometer_producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TermometerProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TermometerProducerApplication.class, args);
	}

}
